import java.net.URL;
import java.util.Collection;

public interface QueryExecutor {
    /**
     * Performs a search query against index
     * @param query free-form text query (words separated by spaces)
     * @param index index to perform a search against
     * @return a list of matching URLs in the decreasing order of rank
     */
    Collection<URL> query(String query, Index index);

}
