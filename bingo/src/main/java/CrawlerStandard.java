import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author sbelov
 */
public class CrawlerStandard implements Crawler {
    private static final int TIMEOUT_VALUE = 10000;

    private static String readURL(URL page) throws IOException {
        URLConnection conn = page.openConnection();
        conn.setConnectTimeout(TIMEOUT_VALUE);
        conn.setReadTimeout(TIMEOUT_VALUE);
        return readString(conn.getInputStream());
    }

    private static String readString(InputStream stream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Crawler crawl(URL firstPage, Index index, Predicate<URL> filter, int maxPages, int depth) throws IOException {
        // TODO implement
        return this;
    }
}
