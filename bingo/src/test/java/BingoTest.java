import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.assertTrue;

public class BingoTest {

    @Test
    public void testCrawl() throws Exception {
        CrawlerStandard crawler = new CrawlerStandard();
        Index index = new IndexStandard();
        crawler.crawl(new URL("http://en.wikipedia.org/wiki/Cosmos"), index, new Predicate<URL>() {
            @Override
            public boolean apply(URL value) {
                return value.getHost().endsWith("en.wikipedia.org") &&
                        (value.getQuery() == null || value.getQuery().trim().length() == 0) &&
                        !value.getPath().contains(":");
            }
        }, 1000, 2);
        QueryExecutor executor = new QueryExecutorStandard();
        assertTrue(executor.query("UFO reports", index).contains(new URL("http://en.wikipedia.org/wiki/Carl_Sagan")));
        assertTrue(executor.query("presence of God", index).contains(new URL("http://en.wikipedia.org/wiki/Christian_theology")));

    }
}